# Hello Module

The hello example used in the golang learning tutorial

## Build

To build this module from source, ensure you have go [installed and functional](https://golang.org/doc/install). Once you can run `go version` with out an error, perform the following steps:


1. Clone this repository

    ```bash
    git clone git@gitlab.com:test-out/go/hello-module
    ```

1. Change the directory to the repository root

    ```bash
    cd hello-module
    ```

1. Install the module

    ```bash
    go install gitlab.com/test-out/go/hello-module
    ```


## Run

To run this module, ensure your environment has the locally built installed module path set within `PATH`

```bash
export PATH=$PATH:$(go list -f '{{.Target}}')
```

Then go ahead and run the program

```bash
hello-module
```

## Credits

This program was written by Richard B Winters, and is strongly based upon the [How to Write Go Code](https://golang.org/doc/code.html) tutorial available on the [Official Go](golang.org) website maintained by Google.


