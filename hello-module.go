/**
 *  @package:    hello-module
 *  @author:     Richard B Winters
 *  @copyright:  2020 Massively Modified, Inc.
 *  @license:    Apache-2.0
 *  @version:    1.0.0
 */
package main

import "fmt"

func main() {
    fmt.Println( "Hello, World." )
}

